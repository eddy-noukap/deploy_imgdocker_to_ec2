## pre-requis: 
- generer une cle ssh en local, puis precisez le nom de la cle dans le fichier var du role ec2
  
ssh-keygen -t rsa -b 1024 -f ~/.ssh/keyname -q -N "" <<< y 2>&1 >/dev/null  : creation de la cle ssh avec suppression du message sortie

## COMMAND

commande 1 : ansible-playbook createInstance.yml --tags create_ec2

commande 2 : ansible-playbook -i 00_inventory.yml deployApp.yml --tags create_ec2
